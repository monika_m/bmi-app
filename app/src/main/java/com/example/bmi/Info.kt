package com.example.bmi

import android.os.Bundle
import android.view.MenuItem
import androidx.appcompat.app.AppCompatActivity;

import kotlinx.android.synthetic.main.activity_info.*

class Info : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_info)

        var bmi= intent.getDoubleExtra("bmi", 0.0) //getting bmi from main activity

        var result_type = ""

        if (bmi==0.0) { //checking in which range the bmi is and viewing a description
            result_description.text=getString(R.string.no_bmi)
        } else if (bmi < 18.5) {
                result_type = getString(R.string.underweight)
                result.setTextColor(resources.getColor(R.color.lapiceLazuri))//lapice lazuri
                result_description.text=getString(R.string.underweight_des)

        } else if (bmi < 25) {
            result_type = getString(R.string.normal)
            result.setTextColor(resources.getColor(R.color.grynszpan))//grynszpan
            result_description.text=getString(R.string.normal_des)
        } else if (bmi < 30) {
            result_type = getString(R.string.overweight)
            result.setTextColor(resources.getColor(R.color.mustard))//mustard
            result_description.text=getString(R.string.overweight_des)
        } else if (bmi < 35) {
            result_type = getString(R.string.obese)
            result.setTextColor(resources.getColor(R.color.brick))//
            result_description.text=getString(R.string.obese_des)
        } else {
            result_type = getString(R.string.extremely_obese)
            result.setTextColor(resources.getColor(R.color.pompeianPink))
            result_description.text=getString(R.string.extremely_obese_des)
        }

        result.setText(bmi.toString() + "\n" + result_type)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {//on pressing back button
        when (item.getItemId()) {
            android.R.id.home -> {
                onBackPressed()
                return true
            }
            else -> {
            }
        }
        return super.onOptionsItemSelected(item)
    }

}
