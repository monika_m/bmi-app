package com.example.bmi

import android.content.Context
import android.content.SharedPreferences
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.recyclerview.widget.LinearLayoutManager
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import kotlinx.android.synthetic.main.activity_last_scores.*
import logic.BmiData
import java.util.*

class LastScores : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_last_scores)

        val sharedPref = getSharedPreferences("last scores", Context.MODE_PRIVATE)
        class Token : TypeToken<MutableList<BmiData>>()
        var scores =sharedPref.getString("scores", null)
        var list= mutableListOf<BmiData>()
        if(scores!=null) {
            list= Gson().fromJson(scores, Token().type)
        }
        list.reverse()

        println(""+list.size+"LIST!"+list)
        recyclerView.layoutManager = LinearLayoutManager(this)
        recyclerView.adapter = ScoresAdapter(list, resources)
    }
}
