package com.example. bmi

import android.content.Context
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.Menu
import android.view.MenuItem
import android.widget.Button
import android.widget.ImageButton
import android.widget.TextView
import android.widget.Toast
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import kotlinx.android.synthetic.main.activity_main.*
import logic.BmiData
import logic.BmiForKgCm
import logic.BmiForPoundInch
import java.text.SimpleDateFormat

import java.util.*


class MainActivity() : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        var bmi =0.0

        var is_imperial = intent.getBooleanExtra("units", false)//check whether units should be English
        if (is_imperial==true){//menu_units.text==kg/cm
            massTV.text= getString(R.string.mass_pound)
            heightTV.text= getString(R.string.height_inches)
        }
        else{
            massTV.text=getString(R.string.mass_kg)
            heightTV.text=getString(R.string.height_cm)
        }

        val button1 = button as Button //setting a "COUNT" button
        button1.setOnClickListener{
            massET.error=null
            heightET.error=null
            //try {
            checkInputs() //check whether the inputs are correct

                if (massET.error == null && heightET.error == null) { //when everything is ok, count BMI, taking appropriate units
                    var mass = massET.text.toString().toInt()
                    var height = heightET.text.toString().toInt()

                    bmi=countBMI(mass, height, is_imperial)
                    checkBMI(bmi, result) //check in which range is counted bmi

                    remember(mass, height, is_imperial)

                    invalidateOptionsMenu()
                }
           // } catch (e: Exception){
           //     Toast.makeText(applicationContext,"Something is wrong! "+e.message, Toast.LENGTH_LONG).show()
            // }
        }//button1

        val info_button = imageButton as ImageButton //set ImageButton - starting Info activity
        info_button.setOnClickListener {
            val intent = Intent(applicationContext, Info::class.java)
            checkInputs()
            if (massET.error == null && heightET.error == null && !result.text.toString().isBlank()) {
                intent.putExtra(
                    "bmi",
                    countBMI(massET.text.toString().toInt(), heightET.text.toString().toInt(), is_imperial)
                ) //pass the bmi value
                startActivity(intent);
            }
        }
    }//onCreate()

    private fun countBMI(mass: Int, height: Int, is_imperial: Boolean):Double{
        if (is_imperial) {
            return ((BmiForPoundInch(mass, height).countBMI() * 100).toInt()).toDouble() / 100
            //show accuracy to 0.01
        } else {
            return ((BmiForKgCm(mass, height).countBMI() * 100).toInt()).toDouble() / 100
        }
    }


    private fun remember(mass: Int, height: Int, is_imperial: Boolean) {
        val sharedPref = getSharedPreferences("last scores", Context.MODE_PRIVATE)
        val editor = sharedPref.edit()
        val currentDate = Calendar.getInstance().time
        val today = SimpleDateFormat("dd.MM.yyyy  HH:mm:ss", Locale.getDefault()).format(currentDate)
        val currentScore= BmiData(mass.toString(), height.toString(), is_imperial.toString(), today)


        class Token : TypeToken<MutableList<BmiData>>()
        var scores =sharedPref.getString("scores", null)
        var list= mutableListOf<BmiData>()
        if(scores!=null) {
           list= Gson().fromJson(scores, Token().type)
        }
        list.add(currentScore)
        println("LIST"+list)

       var list_tosave =listOf<BmiData>()
        if (list.size>10){
            list_tosave= list.drop(1)
        } else
            list_tosave= list.drop(0)

        editor.clear()
        editor.putString("scores",Gson().toJson(list_tosave))
        editor.apply()
    }


    private fun checkInputs(){//checks whether the inputs are correct
        if (massET.text.toString().isBlank() || massET.text.toString().toInt()<=20)
            massET.error=getString(R.string.error_mass) //show error message
        if (heightET.text.toString().isBlank() || heightET.text.toString().toInt()<=20)
            heightET.error=getString(R.string.error_height)

    }

    fun checkBMI(bmi:Double, result: TextView){//check BMI range and show short description in a proper color

        var result_type = ""
        if (bmi < 18.5) {
            result_type = getString(R.string.underweight)
            result.setTextColor(resources.getColor(R.color.lapiceLazuri))//lapice lazuri
        } else if (bmi < 25) {
            result_type = getString(R.string.normal)
            result.setTextColor(resources.getColor(R.color.grynszpan))//grynszpan
        } else if (bmi < 30) {
            result_type = getString(R.string.overweight)
            result.setTextColor(resources.getColor(R.color.mustard))//mustard
        } else if (bmi < 35) {
            result_type = getString(R.string.obese)
            result.setTextColor(resources.getColor(R.color.brick))//
        } else {
            result_type = getString(R.string.extremely_obese)
            result.setTextColor(resources.getColor(R.color.pompeianPink))
        }

        result.setText(bmi.toString() + "\n" + result_type) //set text of result TextView
    }


    override fun onOptionsItemSelected(item: MenuItem) = when (item.itemId) {//"configure" menu options
        R.id.menu_about -> { //start activity About
            val intent = Intent(applicationContext, About::class.java)
            startActivity(intent);
            true
        }
        R.id.menu_units -> {
            var is_imperial= item.title == getString(R.string.units)

            //clear written data
            massET.text.clear()
            heightET.text.clear()
            finish(); //finish activity
            val intent_ =intent
            intent_.putExtra("units", !is_imperial) //change displayed units
            startActivity(intent_) //restart activity
            true
        }
        R.id.menu_lasts ->{
            val intent = Intent(applicationContext, LastScores::class.java)
            startActivity(intent);
            true
        }
        else -> { //action not recognized - call super class fun
            super.onOptionsItemSelected(item)
        }
    }
    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        val inflater = menuInflater
        inflater.inflate(R.menu.menu1, menu) //build a menu
        if (intent.getBooleanExtra("units",false)){ //check which units option to display
            menu.findItem(R.id.menu_units).title= getString(R.string.units)
        }

       onPrepareOptionsMenu(menu)
        return true
    }

    override fun onPrepareOptionsMenu(menu: Menu): Boolean {
        val sharedPref = getSharedPreferences("last scores", Context.MODE_PRIVATE)
        val scores =sharedPref.getString("scores",null)
        if(scores!=null) {
            menu.findItem(R.id.menu_lasts).setEnabled(true)
        } else{
            menu.findItem(R.id.menu_lasts).setEnabled(false)
        }
        return true
    }

    public override fun onSaveInstanceState(savedInstanceState: Bundle) { //save mass and height, e.g. while rotating screen

        if(!massET.text.toString().isBlank())
        savedInstanceState.putInt("mass",massET.text.toString().toInt())
        if (!heightET.text.toString().isBlank())
        savedInstanceState.putInt("height", heightET.text.toString().toInt())

        if (!result.text.toString().isBlank()) {
            savedInstanceState.putString("result", result.text.toString())
            savedInstanceState.putInt("color", result.currentTextColor)
        }

        super.onSaveInstanceState(savedInstanceState)
    }

    public override fun onRestoreInstanceState(savedInstanceState: Bundle) {//restore needed data

        super.onRestoreInstanceState(savedInstanceState)

        val mass = savedInstanceState.getInt("mass")
        val height = savedInstanceState.getInt("height")
        val result_text=savedInstanceState.getString("result")
        val color = savedInstanceState.getInt("color")
        result.setTextColor(color)
        result.text=result_text
        //button.callOnClick() //evaluate result
    }

}


