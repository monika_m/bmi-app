package com.example.bmi

import android.content.res.Resources
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import logic.BmiData
import logic.BmiForKgCm
import logic.BmiForPoundInch


class ScoresAdapter(private var bmis: MutableList<BmiData>, private var resources: Resources) : RecyclerView.Adapter<ScoresAdapter.ViewHolder>() {


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ScoresAdapter.ViewHolder {

        val this_view: View = LayoutInflater.from(parent.context).inflate(R.layout.score, parent, false)

        return ScoresAdapter.ViewHolder(this_view)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
       // println("LIST!"+bmis
        holder.mass.text = bmis[position].mass
        holder.height.text=bmis[position].height
        holder.date.text=bmis[position].date

        val is_imperial =bmis[position].is_imperial.toBoolean()
        if (is_imperial){
            holder.massT.text= resources.getString(R.string.mass_pound)
            holder.heightT.text=resources.getString(R.string.height_inches)
        }

        var bmi=
            if(is_imperial){
                  BmiForPoundInch(bmis[position].mass.toInt(), bmis[position].height.toInt()).countBMI()
                }else{
                BmiForKgCm(bmis[position].mass.toInt(), bmis[position].height.toInt()).countBMI()
            }
        bmi = (bmi*100).toInt().toDouble() / 100
       checkBMI(bmi, holder.result)

    }

    override fun getItemCount() = bmis.size

    class ViewHolder(itemView:View):RecyclerView.ViewHolder(itemView) {
        val massT : TextView = itemView.findViewById(R.id.massT)
        val heightT : TextView = itemView.findViewById(R.id.heightT)
        val mass : TextView = itemView.findViewById(R.id.mass)
        val height : TextView = itemView.findViewById(R.id.height)
        val result : TextView = itemView.findViewById(R.id.resultTV)
        val date : TextView = itemView.findViewById(R.id.date)

    }

    fun checkBMI(bmi:Double, result: TextView){//check BMI range and show short description in a proper color

        var result_type = ""
        if (bmi < 18.5) {
            result_type = resources.getString(R.string.underweight)
            result.setTextColor(resources.getColor(R.color.lapiceLazuri))//lapice lazuri
        } else if (bmi < 25) {
            result_type = resources.getString(R.string.normal)
            result.setTextColor(resources.getColor(R.color.grynszpan))//grynszpan
        } else if (bmi < 30) {
            result_type = resources.getString(R.string.overweight)
            result.setTextColor(resources.getColor(R.color.mustard))//mustard
        } else if (bmi < 35) {
            result_type = resources.getString(R.string.obese)
            result.setTextColor(resources.getColor(R.color.brick))//
        } else {
            result_type = resources.getString(R.string.extremely_obese)
            result.setTextColor(resources.getColor(R.color.pompeianPink))
        }

        result.setText(bmi.toString() + "\n" + result_type) //set text of result TextView
    }

}
