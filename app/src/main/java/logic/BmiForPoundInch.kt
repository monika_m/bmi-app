package logic

class BmiForPoundInch(var mass:Int, var height:Int): BMI {

    override fun countBMI():Double{
        require(mass>20&&height>20){"invalid data"}
        return 703.0*mass/height/height
    }
}