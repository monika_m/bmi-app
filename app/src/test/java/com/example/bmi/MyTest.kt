package com.example.bmi

import logic.BmiForKgCm
import logic.BmiForPoundInch
import org.junit.Assert
import org.junit.Test

class MyTest {

    @Test
    fun for_valid_data_should_return_bmi(){
        val bmi=BmiForKgCm(65,170)
        Assert.assertEquals(22.491, bmi.countBMI(),0.001)
    }

    @Test
    fun for_my_data_should_return_bmi(){
        val bmi=BmiForKgCm(55,172)
        Assert.assertEquals(18.591, bmi.countBMI(),0.001)
    }

    @Test
    fun should_also_work_on_pounds_inches(){
        val bmi =BmiForPoundInch(130,70)
        Assert.assertEquals(18.65, bmi.countBMI(),0.01)
    }
}