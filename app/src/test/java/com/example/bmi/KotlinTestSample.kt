package com.example.bmi

import io.kotlintest.shouldThrow
import io.kotlintest.specs.StringSpec
import logic.BmiForKgCm
import logic.BmiForPoundInch
import org.junit.Assert

class KotlinTestSample : StringSpec() {
    init{
        "my first test - normal path"{
            val bmi=BmiForKgCm(65,170)
            bmi.countBMI() shouldBeAround 22.491
        }

        "exception"{
            val bmi= BmiForKgCm(-1, 10)
            shouldThrow<IllegalArgumentException> {
                bmi.countBMI()
            }
        }

        "exception2"{
            val bmi= BmiForPoundInch(-1, 10)
            shouldThrow<IllegalArgumentException> {
                bmi.countBMI()
            }
        }
        "warunki brzegowe"{
            val bmi=BmiForKgCm(20, 100)
            shouldThrow<IllegalArgumentException> {
                bmi.countBMI()
            }
            val bmi2=BmiForKgCm(21, 100)
            bmi2.countBMI() shouldBeAround 21.0
        }

        "warunki brzegowe2"{
            val bmi=BmiForPoundInch(25, 20)
            shouldThrow<IllegalArgumentException> {
                bmi.countBMI()
            }
            val bmi2=BmiForPoundInch(25, 21)
            bmi2.countBMI() shouldBeAround 39.853
        }

    }

    infix fun Double.shouldBeAround(value: Double){
        Assert.assertEquals(value, this,0.001)
    }
}