package com.example.bmi


import androidx.test.espresso.Espresso.onView
import androidx.test.espresso.action.ViewActions.*
import androidx.test.espresso.assertion.ViewAssertions.matches
import androidx.test.espresso.matcher.ViewMatchers.*
import androidx.test.filters.LargeTest
import androidx.test.rule.ActivityTestRule
import androidx.test.runner.AndroidJUnit4
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith

@LargeTest
@RunWith(AndroidJUnit4::class)
class UITest {

    @Rule
    @JvmField
    var mActivityTestRule = ActivityTestRule(MainActivity::class.java)

    //tests checking whether UI is ok and ranges of BMI are correct
    //Espresso tests do not run neither on my emulator nor my phone with 3 different cables :(

   @Test
   fun visibility_test(){
       val massET = onView(withId((R.id.massET)))
       massET.check(matches(withEffectiveVisibility(Visibility.VISIBLE)))

       val heightET = onView(withId((R.id.heightET)))
       heightET.check(matches(withEffectiveVisibility(Visibility.VISIBLE)))

       val button = onView(withId((R.id.button)))
       button.check(matches(withEffectiveVisibility(Visibility.VISIBLE)))

       val img_button = onView(withId((R.id.imageButton)))
       img_button.check(matches(withEffectiveVisibility(Visibility.VISIBLE)))

       val image = onView(withId((R.id.imageView)))
       image.check(matches(withEffectiveVisibility(Visibility.VISIBLE)))

       val massTV = onView(withId((R.id.massTV)))
       massTV.check(matches(withEffectiveVisibility(Visibility.VISIBLE)))

       val heightTV = onView(withId((R.id.heightTV)))
       heightTV.check(matches(withEffectiveVisibility(Visibility.VISIBLE)))

       val resultTV = onView(withId((R.id.resultTV)))
       resultTV.check(matches(withEffectiveVisibility(Visibility.VISIBLE)))

   }

    @Test
    fun ui_test_normal() {
        val massET = onView(withId((R.id.massET)))
        massET.perform(replaceText("55"), closeSoftKeyboard())

        val heightET = onView(withId((R.id.heightET)))
        heightET.perform(replaceText("172"), closeSoftKeyboard())

        val button = onView(withId((R.id.button)))
        button.perform(click())

        val result = onView(withId((R.id.result)))
        result.check(matches(withText("18.59\nnormal")))
    }

    @Test
    fun ui_test_underweight() {
        val massET = onView(withId((R.id.massET)))
        massET.perform(replaceText("54"), closeSoftKeyboard())

        val heightET = onView(withId((R.id.heightET)))
        heightET.perform(replaceText("172"), closeSoftKeyboard())

        val button = onView(withId((R.id.button)))
        button.perform(click())

        val result = onView(withId((R.id.result)))
        result.check(matches(withText("18.25\nunderweight")))
    }

    @Test
    fun ui_test_overweight() {
        val massET = onView(withId((R.id.massET)))
        massET.perform(replaceText("80"), closeSoftKeyboard())

        val heightET = onView(withId((R.id.heightET)))
        heightET.perform(replaceText("172"), closeSoftKeyboard())

        val button = onView(withId((R.id.button)))
        button.perform(click())

        val result = onView(withId((R.id.result)))
        result.check(matches(withText("27.04\noverweight")))
    }

    @Test
    fun ui_test_obese() {
        val massET = onView(withId((R.id.massET)))
        massET.perform(replaceText("100"), closeSoftKeyboard())

        val heightET = onView(withId((R.id.heightET)))
        heightET.perform(replaceText("172"), closeSoftKeyboard())

        val button = onView(withId((R.id.button)))
        button.perform(click())

        val result = onView(withId((R.id.result)))
        result.check(matches(withText("33.8\nobese")))
    }

    @Test
    fun ui_test__ex_obese() {
        val massET = onView(withId((R.id.massET)))
        massET.perform(replaceText("110"), closeSoftKeyboard())

        val heightET = onView(withId((R.id.heightET)))
        heightET.perform(replaceText("172"), closeSoftKeyboard())

        val button = onView(withId((R.id.button)))
        button.perform(click())

        val result = onView(withId((R.id.result)))
        result.check(matches(withText("37.18\nextremely\nobese")))
    }

}
